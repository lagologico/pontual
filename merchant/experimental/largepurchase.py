s = "07/10/16 - 585 pçs - JKMN ´S COMERCIO DE BRINDES PROMOCIONAIS LTDA."

def replaceCliente(purchase, newCliente):
    purchasePat = re.compile(r'(?P<data>\d\d/\d\d/\d\d) - (?P<qtde>[\d\.]+) pçs - (?P<cliente>.*)')
    m = purchasePat.match(purchase)
    return "{} - {} - {}".format(m.group('data'), m.group('qtde'), newCliente)

def test():
    testeql(replaceCliente(s, "MY CLI"), "07/10/16 - 585 pçs - MY CLI")
