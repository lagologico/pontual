import openpyxl

def numToCell(row, col):
    if col > 26:
        raise ValueError("Column greater than Z not supported")
    return chr(ord('A') + col) + str(row + 1)

def replaceCell(currentFilename, newFilename, row, col, newContents):
    book = openpyxl.load_workbook(currentFilename)
    sheet = book.active
    cellStr = numToCell(row, col)
    
    currentContents = sheet[cellStr].value

    print("Type of contents", type(currentContents))
    print("Current contents", currentContents)

    # replace contents and save
    sheet[cellStr] = newContents
    book.save(filename=newFilename)
    print("Saved to", newFilename)
    
def test():
    TEST_XLSX_FILENAME = "C:/Users/Heitor/Desktop/emacs-24.3/bin/merchant/experimental/data/cardex_report.xlsx"
    TEST_NEW_XLSX_FILENAME = "C:/Users/Heitor/Desktop/emacs-24.3/bin/merchant/experimental/data/cardex_report_uniao.xlsx"
    
    # testeql(numToCell(0, 0), "A1")
    # testeql(numToCell(2, 2), "C3")

    testeql(replaceCell(TEST_XLSX_FILENAME, TEST_NEW_XLSX_FILENAME, 5, 0, "NOVO CLIENTE"), None)
