Programmatic way of Creating, Reading, Updating, and Deleting data

Use the Django shell
$ python manage.py shell

>>> import crud.produto
>>> crud.produto.create("AAB002", "Alicate")

Note: Use a transaction when adding an entire spreadsheet
