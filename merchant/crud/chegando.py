from django.db import IntegrityError
from records.models import Chegando
import crud.produto

# Use only as a reference, it will be confusing to use custom names
# for what amounts to a few lines of code

def create(codigo, qtde, nome):
    try:
        produto = crud.produto.read(codigo)
        if produto:
            chegando = Chegando(produto=produto, qtde=qtde, nome=nome)
            chegando.save()
            return "{} {} saved".format(nome, codigo)
        else:
            raise ValueError("Produto {} does not exist".format(codigo))
    except IntegrityError:
        raise ValueError("{} {} already exists.".format(nome, codigo))

def readCodigo(codigo):
    produto = crud.produto.read(codigo)
    return Chegando.objects.filter(produto=produto)

def readNome(nome):
    return Chegando.objects.filter(nome=nome)

def update(codigo, nome, qtde):
    produto = crud.produto.read(codigo)
    Chegando.objects.filter(produto=produto, nome=nome).update(qtde=qtde)
    return "Updated {} {}".format(codigo, nome)

def delete(codigo, nome):
    try:
        produto = crud.produto.read(codigo)
        chegandos = Chegando.objects.filter(produto=produto, nome=nome).delete()
        return "{} {} deleted".format(nome, codigo)
    except AttributeError:
        return "{} not found".format(codigo)
