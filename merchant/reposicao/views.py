import openpyxl
import re

from io import BytesIO
from openpyxl.writer.excel import save_virtual_workbook

from datetime import date, datetime, timedelta

from django.shortcuts import render
from django.http import HttpResponse
from django.db import transaction
from django.db.models import Sum

from .forms import PreliminaryReportForm, MatchUniaoForm, UnionizeForm

from records.models import Chegando, Pedido, ItemPedido, Produto
from records.models import PedidoUniao, ItemPedidoUniao

def index(request):
    if request.method == "POST":
        form = PreliminaryReportForm(request.POST)
        if form.is_valid():
            return preliminaryReport(request, form.cleaned_data['codigos'])
    else:
        form = PreliminaryReportForm()
        return render(request, 'reposicao/index.html', {'form': form})

@transaction.atomic
def preliminaryReport(request, codigoBangs):
    """A bang! is an exclamation point used to mark codigos that
    should be checked manually"""
    
    # load everything into memory, there isn't that much data
    codigoBangs = list(map(str.upper, codigoBangs.split()))
    
    today = date.today()
    thisYear = today.year
    lastYear = thisYear - 1
    
    oneYearAgo = today - timedelta(days=365)
    lastYearBegin = date(lastYear, 1, 1)
    thisYearBegin = date(thisYear, 1, 1)
    startDate = lastYearBegin

    allVendas = ItemPedido.objects.filter(pedido__data__gte=startDate)
    vendas365 = allVendas.filter(pedido__data__gte=oneYearAgo)
    vendasLastYear = allVendas.filter(pedido__data__gte=lastYearBegin, pedido__data__lt=thisYearBegin)
    vendasThisYear = allVendas.filter(pedido__data__gte=thisYearBegin)

    response_err = ""

    blocks = []
    
    for codigo in codigoBangs:
        if not Produto.objects.filter(codigo=codigo).exists():
            response_err += "Warning: codigo {} not found\n".format(codigo)

    INDEX = 0

    # for codigo in codigos[1911:1917]:  # small subset
    for produto in Produto.objects.filter(site=True).order_by('codigo'):
        codigo = produto.codigo
        codigoDisplay = codigo
        if codigo in codigoBangs:
            codigoDisplay += " (!)"
            
        # vendasProdutoAll = allVendas.filter(produto__codigo=codigo)
        totalVendasLastYear = vendasLastYear.filter(produto__codigo=codigo).aggregate(Sum('qtde')).get('qtde__sum')
        totalVendasThisYear = vendasThisYear.filter(produto__codigo=codigo).aggregate(Sum('qtde')).get('qtde__sum')

        if totalVendasLastYear is None:
            totalVendasLastYear = 0
            
        if totalVendasThisYear is None:
            totalVendasThisYear = 0
            
        # check if produto should be added (7 months' avg sales)

        try:
            produto = Produto.objects.get(codigo=codigo)
        except:
            response_err += "Could not get {}\n".format(codigo)
            break
        
        blocks.append({ 'index': INDEX,
                        'codigo': codigoDisplay,
                        'disponivel': produto.estoque_disp,
                        'reservado': produto.estoque_resv,
                        'chegando': produto.reportChegando(),
                        'totalVendasLastYear': totalVendasLastYear,
                        'totalVendasThisYear': totalVendasThisYear,
                        'caixa': produto.caixa,
                        'produtoNome': produto.nome,
                        
                    })

        INDEX += 1

    return render(request, 'reposicao/preliminaryReport.html', { 'err': response_err, 'blocks': blocks })

def workdayAfter(data):
    # If data is a Friday, return the next Monday
    wkdy = data.weekday()
    if wkdy == 4:
        return data + timedelta(3)
    else:
        return data + timedelta(1)

def workdayBefore(data):
    # If data is a Monday, return the previous Friday
    wkdy = data.weekday()
    if wkdy == 0:
        return data - timedelta(3)
    else:
        return data - timedelta(1)
    
def findUniao(data, qtde, codigo):
    out = ""
    
    itemPedidos = ItemPedidoUniao.objects.filter(pedidoUniao__data=data, qtde=qtde, produto__codigo__iexact=codigo)

    for ip in itemPedidos:
        nome = PedidoUniao.objects.get(pk=ip.pedidoUniao.id).cliente.nome
        out += nome

    if out.strip() == "":
        # Hack to search one day behind and one after
        itemPedidosBefore = ItemPedidoUniao.objects.filter(pedidoUniao__data=workdayBefore(data), qtde=qtde, produto__codigo__iexact=codigo)

        for ip in itemPedidosBefore:
            nome = PedidoUniao.objects.get(pk=ip.pedidoUniao.id).cliente.nome
            out += nome

    if out.strip() == "":
        # Hack to search one day behind and one after
        itemPedidosAfter = ItemPedidoUniao.objects.filter(pedidoUniao__data=workdayAfter(data), qtde=qtde, produto__codigo__iexact=codigo)

        for ip in itemPedidosAfter:
            nome = PedidoUniao.objects.get(pk=ip.pedidoUniao.id).cliente.nome
            out += nome
    
    if out.strip() == "":
        # give up
        
        out = "CLIENTENAOENCONTRADO***************************************************************************"
    return out    
    
def matchUniao(request):
    if request.method == "POST":
        form = MatchUniaoForm(request.POST)
        if form.is_valid():
            dataRaw = form.cleaned_data['data']
            qtde = int(form.cleaned_data['qtde'])
            codigo = form.cleaned_data['codigo']

            dataRaw = "{}/{}/{}".format(dataRaw[0:2], dataRaw[2:4], dataRaw[4:6])
            data = datetime.strptime(dataRaw, "%d/%m/%y")

            response = findUniao(data, qtde, codigo)

            return HttpResponse(response)
    else:
        form = MatchUniaoForm()
        return render(request, 'reposicao/matchuniao.html', { 'form': form })

def numToCell(row, col):
    if col > 26:
        raise ValueError("Column greater than Z not supported")
    return chr(ord('A') + col) + str(row + 1)

def replaceCliente(purchase, newCliente):
    UNIAO = "UNIAO BRINDES IMPORTAÇAO E EXPORTAÇAO EIRELI"
    if purchase.find(UNIAO) > -1:
        return purchase.replace(UNIAO, newCliente)
    else:
        return purchase

def unionize(request):
    if request.method == "POST":
        form = UnionizeForm(request.POST, request.FILES)
        if form.is_valid():
            file_in_memory = request.FILES['file'].read()
            book = openpyxl.load_workbook(filename=BytesIO(file_in_memory))
            sheet = book.active

            # header and footer
            today = datetime.now().strftime("%d/%m/%y") 
            sheet.oddHeader.center.text = "Reposição {}".format(today) 
            sheet.oddFooter.center.text = "Página &[Page] de &N" 
            sheet.evenFooter.center.text = "Página &[Page] de &N" 
            # keep track of codigo as we encounter them
            currentCodigo = ""

            codigoPat = re.compile(r'^\d{1,6}\w*$')
            purchasePat = re.compile(r'(?P<data>\d\d/\d\d/\d\d) - (?P<qtde>[\d\.]+) pçs - (?P<cliente>.*)')
            
            for row in range(sheet.max_row):
                cell = numToCell(row, 0)
                cellValue = sheet[cell].value
                
                if not cellValue:
                    cellValue = ""
                    
                codigoMatch = codigoPat.match(cellValue)
                purchaseMatch = purchasePat.match(cellValue)
                
                if codigoMatch:
                    currentCodigo = codigoMatch.group(0)

                elif purchaseMatch:
                    data = datetime.strptime(purchaseMatch.group('data'), "%d/%m/%y")
                    qtde = int(purchaseMatch.group('qtde').replace(".", ""))
                    sheet[cell] = replaceCliente(sheet[cell].value, findUniao(data, qtde, currentCodigo))
                
            response = HttpResponse(content=save_virtual_workbook(book), content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
            response['Content-Disposition'] = 'attachment; filename=cardex_report_unionized.xlsx'
            return response
    else:
        form = UnionizeForm()
        return render(request, 'reposicao/unionize.html', {'form': form})
