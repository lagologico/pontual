from django.conf.urls import url

from . import views

app_name = 'reposicao'

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^matchuniao/$', views.matchUniao, name='matchUniao'),
    url(r'^unionize/$', views.unionize, name='unionize'),
]
