from django.contrib import admin
from .models import Incluido, Atualizado, UltimoEstoque

admin.site.register(Incluido)
admin.site.register(Atualizado)
admin.site.register(UltimoEstoque)

