import re
import xlrd
from datetime import date, datetime

from django.db import IntegrityError, transaction

from records.models import Produto, Compra, ItemCompra, Atualizado

@transaction.atomic
def create(f):
    """Create Compra and ItemCompras from UploadedFile f"""
    response = ""
    response_err = ""
    
    book = xlrd.open_workbook(file_contents=f.read())
    sheet = book.sheet_by_index(0)

    rows = sheet.nrows
    
    # columns
    CONTAINER_ROW = 0
    CONTAINER_COL = 14
    DATA_ROW = 4
    DATA_COL = 17
    
    CODIGO = 1
    QTDE = 12

    response += "<pre>"
    response_err += "<pre style='color: red;'>"

    compraNome = sheet.cell(CONTAINER_ROW, CONTAINER_COL).value
    compraDataRaw = sheet.cell(DATA_ROW, DATA_COL).value
    compraData = datetime.strptime(compraDataRaw, "%d/%m/%Y").strftime("%Y-%m-%d")
    
    compra, compraCreated = Compra.objects.get_or_create(container=compraNome, data=compraData)
    
    if compraCreated:
        response += "Created compra {}\n".format(compraNome)
    else:
        response += "Compra {} already exists\n".format(compraNome)

    valid_codigo_pat = re.compile(r'\w{5,8}$')

    for ROW in range(1, rows):
        codigoCellValue = sheet.cell(ROW, CODIGO).value
        if isinstance(codigoCellValue, str):
            codigo = codigoCellValue
        else:
            codigo = str(int(codigoCellValue))

        codigo = codigo.strip()
        
        # check if codigo is valid
        if codigo == "" or codigo == 'CÓDIGO':
            continue
            
        if not valid_codigo_pat.match(codigo):
            response_err += "{} is not a valid codigo\n".format(codigo)
            continue

        try:
            produto = Produto.objects.get(codigo=codigo)
        except:
            response_err += "Could not find produto {}, aborting\n".format(codigo)
            break

        try:
            qtde = int(sheet.cell(ROW, QTDE).value)
        except ValueError:
            response_err += "Could not read codigo {}'s qtde. Skipping\n".format(codigoCellValue)
            continue
        
        # create ItemCompra
        try:
            itemCompra, itemCompraCreated = ItemCompra.objects.update_or_create(compra=compra, produto=produto, defaults={'qtde': qtde})
            if itemCompraCreated:
                response += "{} pc {} saved\n".format(qtde, codigo)
            else:
                response += "{} already exists, updating\n".format(codigo)    
            
        except IntegrityError:
            response_err += "IntegrityError in adding or updating {}\n".format(codigo)
    else:
        response = "ALL OK\n" + response
        Atualizado.atualizar('compra')
        
    return response_err + "</pre>" + response


