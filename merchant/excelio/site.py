import re
import xlrd

from django.db import IntegrityError, transaction

from records.models import Produto, Atualizado

@transaction.atomic
def create(f):
    """Create Chegando from UploadedFile f"""
    response = ""
    response_err = ""
    
    book = xlrd.open_workbook(file_contents=f.read())
    sheet = book.sheet_by_index(0)

    rows = sheet.nrows
    
    response += "<pre>"
    response_err += "<pre style='color: red;'>"

    # set all to false
    Produto.objects.all().update(site=False)
    
    valid_codigo_pat = re.compile(r'\w{5,8}$')

    for ROW in range(rows):
        codigoCellValue = sheet.cell(ROW, 0).value
        if isinstance(codigoCellValue, str):
            codigo = codigoCellValue
        else:
            codigo = str(int(codigoCellValue))

        codigo = codigo.strip()
        
        # check if codigo is valid
        if codigo == "":
            continue
            
        if not valid_codigo_pat.match(codigo):
            response_err += "{} is not a valid codigo\n".format(codigo)
            continue

        try:
            produto = Produto.objects.get(codigo=codigo)
        except:
            response_err += "{} not found. Aborting\n".format(codigo)
            break

        # update Produto
        try:
            produto.site = True
            produto.save()
            response += "{} in site\n".format(codigo)
        except:
            response_err += "IntegrityError in updating {}. Aborting\n".format(codigo)
            break
    else:
        response += "ALL OK\n"
        Atualizado.atualizar('site')
        
    return response_err + "</pre>" + response


