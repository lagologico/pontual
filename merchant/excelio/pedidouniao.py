import re
import xlrd
from datetime import date, datetime

from django.db import IntegrityError, transaction

from records.models import PedidoUniao, Cliente, Vendedor, Atualizado

@transaction.atomic
def create(f):
    """Create Pedidos from UploadedFile f"""
    response = ""
    response_err = ""
    
    book = xlrd.open_workbook(file_contents=f.read())
    sheet = book.sheet_by_index(0)

    rows = sheet.nrows
    
    # columns
    NUMERO = 1
    CLIENTE = 3
    DATA = 6

    response += "<pre>"
    response_err += "<pre style='color: red;'>"

    vendedor, vendedorCreated = Vendedor.objects.get_or_create(nome="Pontual")

    for ROW in range(rows):
        numeroCellValue = sheet.cell(ROW, NUMERO).value

        if isinstance(numeroCellValue, str):
            continue

        try:
            numero = int(numeroCellValue)
        except ValueError:
            response_err += "Could not read numero {}. Skipping\n".format(numeroCellValue)
            continue
        
        if numero < 1:
            response_err += "Numero {} too small. Skipping\n".format(numero)
            continue

        nomeCliente = sheet.cell(ROW, CLIENTE).value
        response += "Cliente {}\n".format(nomeCliente)

        dataValue = sheet.cell(ROW, DATA).value

        data = datetime.strptime(dataValue, "%d/%m/%Y").strftime("%Y-%m-%d")
        response += "Data {}\n".format(data)
            

        # create pedido
        try:
            cliente, clienteCreated = Cliente.objects.get_or_create(nome=nomeCliente, defaults={'vendedor': vendedor})
            if clienteCreated:
                response += "Created cliente {}\n".format(nomeCliente)
                
            pedido, pedidoCreated = PedidoUniao.objects.update_or_create(numero=numero, defaults={'cliente': cliente, 'data': data})
            if pedidoCreated:
                response += "{} saved\n".format(numero)
            else:
                response += "{} already exists, updating\n".format(numero)    
            
        except IntegrityError:
            response_err += "IntegrityError in adding or updating {} \n".format(numero)
    else:
        response = "ALL OK\n" + response
        Atualizado.atualizar('pedidouniao')
        
    return response_err + "</pre>" + response
