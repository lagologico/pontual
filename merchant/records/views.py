from datetime import date

from django.shortcuts import render
from django.http import HttpResponse
from .forms import UploadExcelForm

from .models import Atualizado

import excelio.produto
import excelio.site
import excelio.estoque
import excelio.pedido
import excelio.venda
import excelio.pedidouniao
import excelio.vendauniao
import excelio.compra
import excelio.chegando

def dataAtualizado(tipo):
    try:
        data = Atualizado.objects.get(tipo=tipo).data.strftime("%d/%m")
    except Atualizado.DoesNotExist:
        data = "--"
    return data

def index(request):
    tipos = ['produto', 'site', 'estoque', 'pedido', 'venda', 'pedidouniao', 'vendauniao', 'compra', 'itemcompra', 'chegando']
    atualizados = {}
    for tipo in tipos:
        atualizados[tipo] = dataAtualizado(tipo)
        
    return render(request, 'records/index.html', atualizados)

def fileUploadView(request, callback, formTemplate, templateVars):
    # https://docs.djangoproject.com/en/1.11/topics/http/file-uploads/
    if request.method == "POST":
        form = UploadExcelForm(request.POST, request.FILES)
        if form.is_valid():
            response = callback(request.FILES['file'])
            return HttpResponse(response)
    else:
        form = UploadExcelForm()
        templateVars.update({'form': form})
        return render(request, formTemplate, templateVars)

def produto(request):
    tipo = 'produto'
    data = dataAtualizado(tipo)

    return fileUploadView(request, excelio.produto.create, 'records/produto.html', { 'tipo': tipo, 'data': data })
    
def site(request):
    tipo = 'site'
    data = dataAtualizado(tipo)

    return fileUploadView(request, excelio.site.create, 'records/site.html', { 'tipo': tipo, 'data': data })

def estoque(request):
    tipo = 'estoque'
    data = dataAtualizado(tipo)

    return fileUploadView(request, excelio.estoque.update, 'records/estoque.html', { 'tipo': tipo, 'data': data })

def estoque_manual(request):
    return HttpResponse("estoque_manual")

def pedido(request):
    tipo = 'pedido'
    data = dataAtualizado(tipo)

    return fileUploadView(request, excelio.pedido.create, 'records/pedido.html', { 'tipo': tipo, 'data': data })

def venda(request):
    tipo = 'venda'
    data = dataAtualizado(tipo)

    return fileUploadView(request, excelio.venda.create, 'records/venda.html', { 'tipo': tipo, 'data': data })

def pedidoUniao(request):
    tipo = 'pedidouniao'
    data = dataAtualizado(tipo)

    return fileUploadView(request, excelio.pedidouniao.create, 'records/pedidouniao.html', { 'tipo': tipo, 'data': data })

def vendaUniao(request):
    tipo = 'vendauniao'
    data = dataAtualizado(tipo)

    return fileUploadView(request, excelio.vendauniao.create, 'records/vendauniao.html', { 'tipo': tipo, 'data': data })

def compra(request):
    tipo = 'compra'
    data = dataAtualizado(tipo)
    
    return fileUploadView(request, excelio.compra.create, 'records/compra.html', { 'tipo': tipo, 'data': data })

def itemCompra(request):
    return HttpResponse("item compra (use compra instead)")

def chegando(request):
    tipo = 'chegando'
    data = dataAtualizado(tipo)
    
    return fileUploadView(request, excelio.chegando.create, 'records/chegando.html', { 'tipo': tipo, 'data': data })
