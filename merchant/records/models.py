from decimal import Decimal
from datetime import date

from django.db import models

# Create your models here.

class Produto(models.Model):
    codigo = models.CharField(max_length=12, unique=True)
    nome = models.CharField(max_length=80, blank=True)
    caixa = models.IntegerField(default=0)
    estoque_disp = models.IntegerField(default=0)
    estoque_resv = models.IntegerField(default=0)
    preco = models.DecimalField(max_digits=10, decimal_places=4, default=Decimal('0.0000'))
    estoque_last_updated = models.DateField(blank=True, null=True)
    site = models.BooleanField(default=False)

    def reportChegando(self):
        produtoChegandos = Chegando.objects.filter(produto=self)
        lenProdutoChegando = len(produtoChegandos)
        chegandoDisplay = ""
        if lenProdutoChegando > 0:
            for i, pc in enumerate(produtoChegandos):
                chegandoDisplay += "{} ({})".format(str(pc.qtde), pc.nome)
                if i != lenProdutoChegando - 1:
                    chegandoDisplay += ", "
        else:
            chegandoDisplay = "0"
        return chegandoDisplay
        
    def __str__(self):
        return "{} {}".format(self.codigo, self.nome)

class Vendedor(models.Model):
    nome = models.CharField(max_length=80)

    def __str__(self):
        return self.nome

    class Meta:
        verbose_name_plural = "vendedores"
        
class Cliente(models.Model):
    codigo = models.CharField(max_length=8, blank=True)
    nome = models.CharField(max_length=80, unique=True)
    vendedor = models.ForeignKey(Vendedor, null=True, on_delete=models.SET_NULL)
    endereco = models.CharField(max_length=80, blank=True)
    bairro = models.CharField(max_length=80, blank=True)
    cidade = models.CharField(max_length=80, blank=True)
    estado = models.CharField(max_length=2, blank=True)
    cep = models.CharField(max_length=9, blank=True)
    telefone = models.CharField(max_length=16, blank=True)
    celular = models.CharField(max_length=16, blank=True)
    email = models.CharField(max_length=80, blank=True)
    cnpj = models.CharField(max_length=80, blank=True)
    inscricao = models.CharField(max_length=80, blank=True)

    def __str__(self):
        return "{} {}".format(self.codigo, self.nome)

class Chegando(models.Model):
    produto = models.ForeignKey(Produto, null=True, on_delete=models.SET_NULL)
    qtde = models.IntegerField(default=0)
    nome = models.CharField(max_length=32, blank=True)

    def __str__(self):
        if self.produto is None:
            codigo = "cod. null"
        else:
            codigo = self.produto.codigo
        return "{} {} pçs {}".format(self.nome, self.qtde, codigo)

    class Meta:
        unique_together = ('produto', 'nome', 'qtde')

class Pedido(models.Model):
    numero = models.IntegerField(unique=True)
    cliente = models.ForeignKey(Cliente, null=True, on_delete=models.SET_NULL)
    data = models.DateField()
    desconto = models.DecimalField(max_digits=5, decimal_places=4, default=Decimal('0.0000'))

    def clienteFinal(self):
        """Returns the actual client of the pedido, if it's Uniao.
        Otherwise, return the same cliente."""
        nomeCliente = self.cliente.nome
        if nomeCliente[:20] == "UNIAO BRINDES IMPORT":
            # logic to search PedidoUniao
            # get first ItemPedido
            itemPedido = ItemPedido.objects.filter(pedido__numero=self.numero).first()
            data = itemPedido.pedido.data
            qtde = itemPedido.qtde
            codigo = itemPedido.produto.codigo
            
            itemPedidoUniao = ItemPedidoUniao.objects.filter(pedidoUniao__data=data, qtde=qtde, produto__codigo__iexact=codigo).first()

            nomeClienteFinal = PedidoUniao.objects.get(pk=itemPedidoUniao.pedidoUniao.id).cliente.nome

            return nomeClienteFinal
        else:
            return nomeCliente

    def __str__(self):
        return "{} {}: {}".format(self.data, self.numero, self.cliente)

class ItemPedido(models.Model):
    pedido = models.ForeignKey(Pedido, null=True, on_delete=models.SET_NULL)
    produto = models.ForeignKey(Produto, null=True, on_delete=models.SET_NULL)
    qtde = models.IntegerField(default=0)

    def __str__(self):
        if self.produto is None:
            codigo = "cod. null"
        else:
            codigo = self.produto.codigo
        return "{}: {} pçs {}".format(self.pedido.numero, self.qtde, codigo)

    class Meta:
        unique_together = ('pedido', 'produto')

class Compra(models.Model):
    container = models.CharField(max_length=16, unique=True)
    data = models.DateField()

    def __str__(self):
        return self.container

class ItemCompra(models.Model):
    compra = models.ForeignKey(Compra, null=True, on_delete=models.SET_NULL)
    produto = models.ForeignKey(Produto, null=True, on_delete=models.SET_NULL)
    qtde = models.IntegerField(default=0)

    def __str__(self):
        if self.produto is None:
            codigo = "cod. null"
        else:
            codigo = self.produto.codigo

        return "{} {} pçs {}".format(self.compra, self.qtde, codigo)

    class Meta:
        unique_together = ('compra', 'produto')

class PedidoUniao(models.Model):
    numero = models.IntegerField(unique=True)
    cliente = models.ForeignKey(Cliente, null=True, on_delete=models.SET_NULL)
    data = models.DateField()
    desconto = models.DecimalField(max_digits=5, decimal_places=4, default=Decimal('0.0000'))
    
    def __str__(self):
        return "{} {}: {}".format(self.data, self.numero, self.cliente)

    class Meta:
        verbose_name_plural = 'Pedidos Uniao'
        
class ItemPedidoUniao(models.Model):
    pedidoUniao = models.ForeignKey(PedidoUniao, null=True, on_delete=models.SET_NULL)
    produto = models.ForeignKey(Produto, null=True, on_delete=models.SET_NULL)
    qtde = models.IntegerField(default=0)

    def __str__(self):
        if self.produto is None:
            codigo = "cod. null"
        else:
            codigo = self.produto.codigo
        return "{}: {} pçs {}".format(self.pedidoUniao.numero, self.qtde, codigo)

    class Meta:
        unique_together = ('pedidoUniao', 'produto')
        verbose_name_plural = 'Item pedidos Uniao'

class Atualizado(models.Model):
    tipo = models.CharField(max_length=32)
    data = models.DateField()

    @classmethod
    def atualizar(cls, tipo):
        atualizado, atualizadoCreated = cls.objects.update_or_create(tipo=tipo, defaults={ 'data': date.today().strftime("%Y-%m-%d") })

    def atualizarAgora(self):
        self.data = date.today().strftime("%Y-%m-%d")
        self.save()
        
    def __str__(self):
        return "{} {}".format(self.data, self.tipo)
