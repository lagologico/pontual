<?php
require_once('../db.php');
include("header.php");

$dbh->prepare("create table ultimo_estoque
(codigo varchar(16),
estoque int,
constraint pk_ultimo_estoque primary key (codigo)) engine=InnoDB;")->execute();

$dbh->prepare("create table site_produtos_estoque_update
(codigo varchar(16),
updated date,
constraint pk_codigo primary key (codigo)) engine=InnoDB;")->execute();

$dbh->prepare("create table last_update
(datatype varchar(32),
updated datetime not null default current_timestamp on update current_timestamp);")->execute();

$dbh->prepare("create table produto
(codigo varchar(16),
nome varchar(80),
disp int,
resv int,
caixa int,
constraint pk_produto primary key (codigo)) engine=InnoDB;")->execute();

// $dbh->prepare("create table cliente
// (id varchar(16),
// nome varchar(80),
// constraint pk_cliente primary key (id)) engine=InnoDB;")->execute();

$dbh->prepare("create table pedido
(numero int,
data date,
cliente varchar(120),
constraint pk_pedido primary key (numero)) engine=InnoDB;")->execute();

$dbh->prepare("create table venda
(numero int,
codigo varchar(16),
qtde int,
constraint pk_venda primary key (numero, codigo, qtde),
constraint fk_venda_pedido foreign key (numero) references pedido (numero),
constraint fk_venda_produto foreign key (codigo) references produto (codigo)) engine=InnoDB;")->execute();

$dbh->prepare("create table pac
(codigo varchar(16),
qtde int,
nome varchar(32),
constraint pk_pac primary key (codigo, qtde, nome),
constraint fk_pac_produto foreign key (codigo) references produto (codigo)) engine=InnoDB;")->execute();

$dbh->prepare("create table entrada
(nome varchar(32),
data date,
constraint pk_entrada primary key (nome, data)) engine=InnoDB;")->execute();

$dbh->prepare("create table compra
(data date,
container varchar(32),
codigo varchar(16),
qtde int,
constraint pk_compra primary key (data, container, codigo, qtde),
constraint fk_compra_produto foreign key (codigo) references produto (codigo)) engine=InnoDB;")->execute();

$dbh->prepare("create table incluido
(codigo varchar(16),
data date,
constraint pk_incluido primary key (codigo, data),
constraint fk_incluido_produto foreign key (codigo) references produto (codigo)) engine=InnoDB;")->execute();
