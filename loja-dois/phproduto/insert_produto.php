<?php
require_once("../db.php");

$json = file_get_contents("php://input");
$sheet = json_decode($json, true);

// print_r($obj);
$lastRow = (int) $sheet['lastRow'];

$stmt = $dbh->prepare("insert into produto (codigo, nome, disp, resv, caixa) values (:codigo, :nome, :disp, :resv, :caixa)");

$dbh->beginTransaction();

echo $lastRow;
echo "\n";

for ($r = 1; $r <= $lastRow; $r++) {
    if (isset($sheet[$r . '-2'])) {
        $produto_nome = $sheet[$r . '-2'];

        $rowCodigo = $sheet[$r . '-1'];
        $rowNome = $sheet[$r . '-2']; 

        if (isset($sheet[$r . '-6'])) {
            $rowDisp = $sheet[$r . '-6'];
            $rowResv = $sheet[$r . '-9'];
        } else {
            $rowDisp = 0;
            $rowResv = 0;
        }            
        
        echo "Attempt to insert $rowCodigo $rowNome\n";

        try {
            $stmt->execute([':codigo' => $rowCodigo,
                            ':nome' => $rowNome,
                            ':disp' => $rowDisp,
                            ':resv' => $rowResv,
                            ':caixa' => 0]);
        } catch (Exception $e) {
            echo $e;
        }
        
        ob_flush();
        // insertProduto($dbh, $rowCodigo, $rowNome, $rowDisp, $rowResv, 0);
    }
}

$dbh->commit();

addTimestamp($dbh, "insertProduto");
