<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title>XLSX to Array Demo</title>
  </head>
  <body>
     <?php
     require_once("../db.php");
include_once("header.php");
echo "Last Insert Compra: ";
echo getTimestamp($dbh, "insertCompra");
?>

<br><br>

<!-- USING insert_compra_from_pac.php -->

<!--
AVOID THE INACCURATE COMPRAS DATA (WHICH ARE ALWAYS BEFORE CONTAINER ARRIVAL DATE

Compras por data > Analitico<br> -->

<!--
MANUALLY GENERATE DATA FROM PAC SCREEN -> IMPRIMIR
<br>
Formato:
<br>
CCTC/???? ; Date (in top-right corner) e.g. '15/01/16 ; Codigo ; Qtde
<br>
-->

PAC section > Imprimir > Save as Excel
<br><br>
<a href="list_compra_containers.php">List Compra PAC Containers</a>
<br><br>
     <input type="file" id="fileInput">

    <pre id="out">
    </pre>
    <script src="lib/js/xlsx.core.min.js"></script>
    <script src="lib/js/jquery-2.1.4.min.js"></script>
    <script src="xlsx-to-array.js"></script>
    <script src="insert_compra.js"></script>
  </body>
</html>
