<head>
<meta charset="utf-8">
     <style>
     td { 
     white-space: nowrap;
     }

     .ctr {
     text-align: center;
     }
</style>
</head>
     <body>
     <?php
     require_once("header.php");
     require_once("config.php");

require_once("../db.php");

// insert all_codigos.php for list of site codigos
// $cods = ["137350A",
//         "143214"];

require_once("filter.php");

     $cods = $filter_first_pass;
     
?>

<form action="report_xlsx.php" method="post">

  <input type="button" value="Check all" onclick="checkAll();">
  
<table border="0" width="10%">

<?php
require_once("sqls_for_report.php");

$ultimo_estoque_sql = 'select estoque from ultimo_estoque where codigo = :codigo';

$incluido = 'select data as raw_date, date_format(data, "%d/%m/%y") as data from incluido
where codigo = :codigo
order by raw_date desc limit 3';

$elts = [];

// Is out of stock at this point in time
$is_out = false;

$ct = 0;
foreach ($cods as $cod) {
    $ct++;
    $elts[$cod] = [];

    $is_out = false;

    $stmt = $dbh->prepare($pac);
    $stmt->execute([':codigo' => $cod]);
    $row = $stmt->fetch();

    if (empty(count($row["pac_text"]))) {
        $elts[$cod]["pac"] = 0;
    } else {
        $elts[$cod]["pac"] = $row["pac_text"];
    }

    /* leave filtering to filter.php
    if (substr($elts[$cod]["pac"], 0, 3) === "0 (") {
        continue;
    }
    */
    
    $stmt = $dbh->prepare($prodinfo);
    $stmt->execute([':codigo' => $cod]);
    $row = $stmt->fetch();

    $elts[$cod]["codigo"] = $cod;
    $elts[$cod]["disp"] = $row["disp"];
    $elts[$cod]["resv"] = $row["resv"];

    $disp_num = (int) str_replace(".", "", $row["disp"]);
    $resv_num = (int) str_replace(".", "", $row["resv"]);
    $elts[$cod]["stock"] = $disp_num + $resv_num;
    
    $elts[$cod]["caixa"] = $row["caixa"];
    $elts[$cod]["nome"] = $row["nome"];

    if ($row["caixa"] == 0) {
        echo "<br><br><font color='red'>WARNING: Qtde por caixa is Zero for cod. $cod</font><br><br>";
    }

    $stmt = $dbh->prepare($incluido);
    $stmt->execute([':codigo' => $cod]);
    $rows = $stmt->fetchAll();

    $incluidos = array_column($rows, "data");
    $elts[$cod]["incluido"] = implode("; ", $incluidos);

    $stmt = $dbh->prepare($ultimo_estoque_sql);
    $stmt->execute([':codigo' => $cod]);
    $row = $stmt->fetch();

    $elts[$cod]["ultimo_estoque"] = $row['estoque'];

    $stmt = $dbh->prepare($vendas);
    $stmt->execute([':codigo' => $cod]);
    $row = $stmt->fetch();

    $elts[$cod]["vendas2017"] = str_replace(".", "", $row["vendas2017"]);
    $elts[$cod]["vendas2018"] = str_replace(".", "", $row["vendas2018"]);

    if (empty($elts[$cod]["vendas2017"])) {
        $elts[$cod]["vendas2017"] = 0;
    }
    if (empty($elts[$cod]["vendas2018"])) {
        $elts[$cod]["vendas2018"] = 0;
    }

    $stmt = $dbh->prepare($ultimo);
  $stmt->execute([':codigo1' => $cod]);
  
  // ':codigo2' => $cod]);
    $row = $stmt->fetch();

    $elts[$cod]["ultimo"] = $row["ultimo"];
    if (empty($elts[$cod]["ultimo"])) {
        $elts[$cod]["ultimo"] = "";
    } else {
        $elts[$cod]["ultimo"] .= "\n";
    }

  // Ultimo container Calculation is incorrect, do not compute with sql.
  // fill in manually
  // $elts[$cod]["ultimo"] = "Último container - ";

    $stmt = $dbh->prepare($largest);
    $stmt->execute([':codigo' => $cod]);
    $rows = $stmt->fetchAll();

    $largest_vendas = array_column($rows, "venda");
    $elts[$cod]["largest"] = implode('</td></tr><tr><td colspan="6">', $largest_vendas);

    $stmt = $dbh->prepare($curstock);
    $stmt->execute([':codigo' => $cod]);
    $row = $stmt->fetch();

    $elts[$cod]["curstock"] = $row["curstock"];
    
    $stmt = $dbh->prepare($stockhist);
    $stmt->execute([':codigo1' => $cod,
                    ':codigo2' => $cod,
                    ':codigo3' => $cod]);
    $rows = $stmt->fetchAll();

    $outofstock_lines = "";
    
    foreach ($rows as $row) {
        // threshold for being out of stock.
        $threshold = $_CONFIG_THRESHOLD;

        // echo $row['d'] . ' ' . $row['q'];
        $elts[$cod]["curstock"] -= $row['q'];
        
        // echo ' ' . $elts[$cod]["curstock"];

        if ($is_out) {
            if ($elts[$cod]["curstock"] > $threshold) {
                $is_out = false;
                $display_date = ISOToDmy($current_end_date);
                if ($display_date == date("d/m/y")) {
                    $display_date = "agora";
                }
                    
                $outofstock_lines .= "Sem estoque de " . ISOToDmy($row['d']) .
                    " até " . $display_date . "\n";
            }
        }
        
        if ($elts[$cod]["curstock"] < $threshold && !$is_out) {
            $is_out = true;
            // echo " OUT OF STOCK HERE";
            $current_end_date = $row['d'];
        }
        // echo "\n";
    }

    $elts[$cod]["outofstock"] = $outofstock_lines;
    
    echo report_elt($elts, $cod, $is_excl);
    
    // echo "Trying $cod\n";
    // print_r($row);

    // if ($row["caixa"] < 1) {
    // echo "$row[caixa] $row[codigo] $row[disp] $row[resv] $row[caixa] $row[nome]<br>";
    // }
}

  function report_elt($elts, $codigo, $is_excl) {

  $formatted_stock = number_format($elts[$codigo]['stock'], 0, ",", ".");
  $formatted_vendas2017 = number_format($elts[$codigo]['vendas2017'], 0, ",", ".");
  $formatted_vendas2018 = number_format($elts[$codigo]['vendas2018'], 0, ",", ".");

    return <<<_END
<tr>
<td colspan="6">
    <input type="checkbox" name="selected_cod[]" id="checkbox$codigo" value="$codigo" onfocus="document.getElementById('foto').src='fotos/$codigo.jpg'">
  <label for="checkbox$codigo">$codigo</label>

    <span onmouseover="document.getElementById('foto').src='fotos/$codigo.jpg'"><u>ver foto</u></span> {$is_excl[$codigo]}

    </td>
    </tr>
    <tr>
      <td colspan="6">
      <font color="blue">Últ estoq: {$elts[$codigo]['ultimo_estoque']}</font>
      </td>
      </tr>
      
      <tr>
      <td colspan="6">
      <font color="blue">Incluido: {$elts[$codigo]['incluido']}</font>
      </td>
</tr>

  <tr>

    <td>$codigo </td>
    <td class="ctr"> $formatted_stock </td>
    <td class="ctr"> {$elts[$codigo]['pac']} </td>
    <td class="ctr"> $formatted_vendas2017 </td>
    <td class="ctr"> $formatted_vendas2018 </td>
    <td class="ctr"> {$elts[$codigo]['caixa']}</td>
<tr>
    <td colspan="6">
{$elts[$codigo]['nome']}
</td></tr>

<tr>
    <td colspan="6">
{$elts[$codigo]['ultimo']}
</td>
</tr>
<tr>
    <td colspan="6">
<pre>{$elts[$codigo]['outofstock']}</pre>
</td>
</tr>
<tr>
<td colspan="6">
{$elts[$codigo]['largest']}
</td></tr>

<tr><td>-----</td></tr>

_END;
}
?>

<tr>
    <td colspan="6">
<input type="submit">
</td></tr>
</table>
    </form>

    <img height="200" src="fotos/blank.jpg" style="position: fixed; top: 0; right: 0;" id="foto">

    <script>
      function checkAll() {
        var inputs = document.getElementsByTagName("input");
        for (var i = 0; i < inputs.length; i++) {
          if (inputs[i].type == "checkbox") {
            inputs[i].checked = true;
          }
        }
      }
    </script>
    </body>
