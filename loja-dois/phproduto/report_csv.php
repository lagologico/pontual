<?php

require_once("../db.php");

// insert all_codigos.php for list of site codigos
// $cods = ["137350A",
//         "143214"];

mb_internal_encoding("UTF-8");

$cods = $_POST['selected_cod'];

$output = "Código,Estoque disponível,Estoque reservado,Chegando (n. container),Vendas 2017,Vendas 2018,Cx. grande\n";

$prodinfo = 'select prod.codigo, format(prod.disp, 0, "de_DE") as disp, format(prod.resv, 0, "de_DE") as resv, format(prod.caixa, 0, "de_DE") as caixa, prod.nome
from produto as prod
where prod.codigo = :codigo';

$pac = 'select concat(format(sum(qtde), 0, "de_DE"), " (", nome, ")") as pac_text from pac where codigo = :codigo';

$vendas = 'select prod.codigo,
format(sum(case when pedido.data between "2017-01-01" and "2017-12-31" then venda.qtde end), 0, "de_DE") as vendas2017,
format(sum(case when pedido.data >= "2018-01-01" then venda.qtde end), 0, "de_DE") as vendas2018
from produto as prod, pedido, venda
where pedido.numero = venda.numero
and venda.codigo = prod.codigo
and prod.codigo = :codigo';

$ultimo = 'select concat("Último container - ", date_format(earliest_entrada.data, "%d/%m/%y"), " - ", earliest_entrada.nome, " - ", format(last_compra.qtde, 0, "de_DE"), " pçs") as ultimo
from (select * from entrada where data > (select max(data) from compra where codigo = :codigo1) order by data asc limit 1) as earliest_entrada, (select * from compra where codigo = :codigo1 order by data desc limit 1) as last_compra';

$largest = 'select venda from (select pedido.data as data,
concat(date_format(pedido.data,
"%d/%m/%y"), " - ", format(venda.qtde, 0, "de_DE"), " pçs", " - ",
substring_index(pedido.cliente, " ", 6))
as venda
from pedido, venda
where pedido.numero = venda.numero
and venda.codigo = :codigo
and pedido.data >= "2017-01-01"
order by venda.qtde desc limit 3) as formatted order by data desc';

$curstock = 'select (disp + resv) as curstock from produto
where codigo = :codigo';

$stockhist = 'select current_date as d, 0 as q
from produto where codigo = :codigo1
union all
select pedido.data as d, -sum(venda.qtde) as q
from pedido, venda
where pedido.numero = venda.numero
and venda.codigo = :codigo2
group by d
union all
select data as d, qtde as q
from compra
where codigo = :codigo3
order by d desc';

$elts = [];

// Is out of stock in this point in time
$is_out = false;

$ct = 0;
foreach ($cods as $cod) {
    $ct++;
    $elts[$cod] = [];

    $is_out = false;

    try {
        // include codigo to incluido, marking today's date
        $stmt = $dbh->prepare("insert into incluido (codigo, data) values (:codigo, current_date())");
        $stmt->execute([':codigo' => $cod]);
    } catch (Exception $e) {

    }
    
    $stmt = $dbh->prepare($pac);
    $stmt->execute([':codigo' => $cod]);
    $row = $stmt->fetch();

    if (empty(count($row["pac_text"]))) {
        $elts[$cod]["pac"] = 0;
    } else {
        $elts[$cod]["pac"] = $row["pac_text"];
    }

    /* leave filtering to filter.php
    if (substr($elts[$cod]["pac"], 0, 3) === "0 (") {
        continue;
    }
    */
    
    $stmt = $dbh->prepare($prodinfo);
    $stmt->execute([':codigo' => $cod]);
    $row = $stmt->fetch();

    $elts[$cod]["codigo"] = $cod;
    $elts[$cod]["disp"] = $row["disp"];
    $elts[$cod]["resv"] = $row["resv"];
    $elts[$cod]["caixa"] = $row["caixa"];
    $elts[$cod]["nome"] = uc_first_lower($row["nome"]);

    if ($row["caixa"] == 0) {
        // echo "<br><br><font color='red'>WARNING: Zero caixa for cod. $cod</font><br><br>";
        $output .= "\nWARNING: Zero caixa for cod. $cod\n\n";
    }

    $stmt = $dbh->prepare($vendas);
    $stmt->execute([':codigo' => $cod]);
    $row = $stmt->fetch();

    $elts[$cod]["vendas2017"] = $row["vendas2017"];
    $elts[$cod]["vendas2018"] = $row["vendas2018"];

    if (empty($elts[$cod]["vendas2017"])) {
        $elts[$cod]["vendas2017"] = 0;
    }
    if (empty($elts[$cod]["vendas2018"])) {
        $elts[$cod]["vendas2018"] = 0;
    }

    $stmt = $dbh->prepare($ultimo);
    $stmt->execute([':codigo1' => $cod,
                       ':codigo2' => $cod]);
    $row = $stmt->fetch();

    $elts[$cod]["ultimo"] = $row["ultimo"];
    if (empty($elts[$cod]["ultimo"])) {
        $elts[$cod]["ultimo"] = "";
    } else {
        $elts[$cod]["ultimo"] .= "\n";
    }

    $stmt = $dbh->prepare($largest);
    $stmt->execute([':codigo' => $cod]);
    $rows = $stmt->fetchAll();

    $largest_vendas = array_column($rows, "venda");
    $elts[$cod]["largest"] = implode("\n", array_map("uc_first_lower", $largest_vendas));

    $stmt = $dbh->prepare($curstock);
    $stmt->execute([':codigo' => $cod]);
    $row = $stmt->fetch();

    $elts[$cod]["curstock"] = $row["curstock"];
    
    $stmt = $dbh->prepare($stockhist);
    $stmt->execute([':codigo1' => $cod,
                    ':codigo2' => $cod,
                    ':codigo3' => $cod]);
    $rows = $stmt->fetchAll();

    $outofstock_lines = "";
    
    foreach ($rows as $row) {
        // threshold for being out of stock.
        $threshold = 12;

        // echo $row['d'] . ' ' . $row['q'];
        $elts[$cod]["curstock"] -= $row['q'];
        
        // echo ' ' . $elts[$cod]["curstock"];

        if ($is_out) {
            if ($elts[$cod]["curstock"] > $threshold) {
                $is_out = false;
                $outofstock_lines .= "Sem estoque de " . ISOToDmy($row['d']) .
                    " até " . ISOToDmy($current_end_date) . "\n";
            }
        }
        
        if ($elts[$cod]["curstock"] < $threshold && !$is_out) {
            $is_out = true;
            // echo " OUT OF STOCK HERE";
            $current_end_date = $row['d'];
        }
        // echo "\n";
    }

    $elts[$cod]["outofstock"] = $outofstock_lines;

    // ECHO TO BROWSER
    // echo report_elt($elts, $cod);
    $output .= report_elt($elts, $cod);
    
    // echo "Trying $cod\n";
    // print_r($row);

    // if ($row["caixa"] < 1) {
    // echo "$row[caixa] $row[codigo] $row[disp] $row[resv] $row[caixa] $row[nome]<br>";
    // }
}

function report_elt($elts, $codigo) {
    return <<<_END
$codigo,"{$elts[$codigo]['disp']}","{$elts[$codigo]['resv']}",{$elts[$codigo]['pac']},"{$elts[$codigo]['vendas2017']}","{$elts[$codigo]['vendas2018']}","{$elts[$codigo]['caixa']}"
{$elts[$codigo]['nome']}
{$elts[$codigo]['ultimo']}{$elts[$codigo]['outofstock']}{$elts[$codigo]['largest']}


_END;
}

function uc_first_lower($s) {
    return ucwords(mb_strtolower($s));
}

header("Content-Type: application/csv");
header("Content-Disposition: attachment; filename=cardex.csv");
header("Pragma: no-cache");
$output = mb_convert_encoding($output, 'UTF-8', 'auto');
echo $output;
