<?php

$prodinfo = 'select prod.codigo, format(prod.disp, 0, "de_DE") as disp, format(prod.resv, 0, "de_DE") as resv, format(prod.caixa, 0, "de_DE") as caixa, prod.nome
from produto as prod
where prod.codigo = :codigo';

$prodraw = 'select prod.codigo, prod.disp as disp, prod.resv as resv, format(prod.caixa, 0, "de_DE") as caixa, prod.nome
from produto as prod
where prod.codigo = :codigo';

$pac = 'select group_concat(concat(format(qtde, 0, "de_DE"), " (", nome, ")") order by nome) as pac_text from pac where codigo = :codigo';

// SUM OF ALL CHEGANDO $pac = 'select concat(format(sum(qtde), 0, "de_DE"), " (", group_concat(distinct(nome) order by nome), ")") as pac_text from pac where codigo = :codigo';

$vendas = 'select prod.codigo,
format(sum(case when pedido.data between "2017-01-01" and "2017-12-31" then venda.qtde end), 0, "de_DE") as vendas2017,
format(sum(case when pedido.data >= "2018-01-01" then venda.qtde end), 0, "de_DE") as vendas2018
from produto as prod, pedido, venda
where pedido.numero = venda.numero
and venda.codigo = prod.codigo
and prod.codigo = :codigo';

// original, uses estimate between compra and container
// $ultimo = 'select concat("Último container - ", date_format(earliest_entrada.data, "%d/%m/%y"), " - ", earliest_entrada.nome, " - ", format(last_compra.qtde, 0, "de_DE"), " pçs") as ultimo
// from (select * from entrada where data > (select max(data) from compra where codigo = :codigo1) order by data asc limit 1) as earliest_entrada, (select * from compra where codigo = :codigo1 order by data desc limit 1) as last_compra';

// new, uses PAC and requires an exact date match
// $ultimo = 'select concat("Último container - ", date_format(earliest_entrada.data, "%d/%m/%y"), " - ", earliest_entrada.nome, " - ", format(last_compra.qtde, 0, "de_DE"), " pçs") as ultimo
// from (select * from entrada where data = (select max(data) from compra where codigo = :codigo1) order by data asc limit 1) as earliest_entrada, (select * from compra where codigo = :codigo1 order by data desc limit 1) as last_compra';

$ultimo = 'select concat("Último container - ", date_format(data, "%d/%m/%y"), " - ", container, " - ", format(qtde, 0, "de_DE"), " pçs") as ultimo
from compra where codigo = :codigo1 order by data desc limit 1';

$largest = 'select venda from (select pedido.data as data,
concat(date_format(pedido.data,
"%d/%m/%y"), " - ", format(sum(venda.qtde), 0, "de_DE"), " pçs", " - ",
pedido.cliente)
as venda
from pedido, venda
where pedido.numero = venda.numero
and venda.codigo = :codigo
and pedido.data >= "2017-01-01"
group by pedido.cliente, data
order by venda.qtde desc, pedido.data desc limit 3) as formatted order by data desc';

$curstock = 'select (disp + resv) as curstock from produto
where codigo = :codigo';

$stockhist = 'select current_date as d, 0 as q
from produto where codigo = :codigo1
union all
select pedido.data as d, -sum(venda.qtde) as q
from pedido, venda
where pedido.numero = venda.numero
and venda.codigo = :codigo2
and pedido.data > "2017-01-01"
group by d
union all
select data as d, qtde as q
from compra
where codigo = :codigo3
and data > "2017-01-01"
order by d desc';

