<?php
require_once("../db.php");

$json = file_get_contents("php://input");
$sheet = json_decode($json, true);

// CLEAR PAC TABLE (to avoid keeping track of which containers already arrived
$dbh->prepare("truncate table pac")->execute();

$lastRow = (int) $sheet['lastRow'];

$stmt = $dbh->prepare("insert into pac (codigo, qtde, nome) values (:codigo, :qtde, :nome)");

$dbh->beginTransaction();

echo $lastRow;
echo "\n";

for ($r = 1; $r <= $lastRow; $r++) {
    if (isset($sheet[$r . '-2'])) {
        $pacCodigo = $sheet[$r . '-2'];
        $pacNome = $sheet[$r . '-3'];
        $pacQtde = $sheet[$r . '-4'];

        echo "Attempt to insert $pacCodigo $pacNome $pacQtde\n";

        try {
            $stmt->execute([':codigo' => $pacCodigo,
                            ':qtde' => $pacQtde,
                            ':nome' => $pacNome]);
        } catch (Exception $e) {
            echo $e;
        }
        
        ob_flush();
    }
}

$dbh->commit();

addTimestamp($dbh, "insertPac");
