<pre><a href="update_produto_stock_form.php">Back</a></pre>

<?php
require_once("../db.php");

$stmt = $dbh->prepare('update produto set disp = :disp, resv = :resv where codigo = :codigo');

$site_update_stmt = $dbh->prepare("update site_produtos_estoque_update set updated = :updated where codigo = :codigo");

$dbh->beginTransaction();

foreach ($_POST['codigo'] as $codigo) {
  $disp = $_POST['disp'][$codigo];
  $resv = $_POST['resv'][$codigo];

  if (!is_numeric($disp)) {
    $disp = 0;
  }

  if (!is_numeric($resv)) {
    $resv = 0;
  }

  echo "$codigo $disp $resv <br>";
  try {
    $stmt->execute([':disp' => $disp,
                    ':resv' => $resv,
                    ':codigo' => $codigo]);
  } catch (Exception $e) {
    echo $e;
  }

  try {
    // XXX Fix date later
    $site_update_stmt->execute([':updated' => date("Y-m-d"),
                                ':codigo' => $codigo]);
  } catch (Exception $e) {
    echo $e;
  }
}

$dbh->commit();

?>

  <pre><a href="update_produto_stock_form.php">Back</a></pre>
