<meta charset="utf-8">
<?php

require_once("../db.php");

// see all_codigos.php for list of site codigos

// $cods = ["143143A"];  // no stock currently
$cods = ["143143A"];  // no stock currently
// $cods = ["143208A"];  // has printout
// $cods = ["143187A"];  // no stock more than once

$curstock = 'select (disp + resv) as curstock from produto
where codigo = :codigo';

$stockhist = 'select current_date as d, 0 as q
from produto where codigo = :codigo1
union all
select pedido.data as d, -sum(venda.qtde) as q
from pedido, venda
where pedido.numero = venda.numero
and venda.codigo = :codigo2
group by d
union all
select data as d, qtde as q
from compra
where codigo = :codigo3
order by d desc';

?>
<pre>
<?php
$elts = [];

$ct = 0;
$is_out = false;

foreach ($cods as $cod) {
    $ct++;
    $elts[$cod] = [];
    $is_out = false;

    $stmt = $dbh->prepare($curstock);
    $stmt->execute([':codigo' => $cod]);
    $row = $stmt->fetch();

    $elts[$cod]["curstock"] = $row["curstock"];
    
    $stmt = $dbh->prepare($stockhist);
    $stmt->execute([':codigo1' => $cod,
                    ':codigo2' => $cod,
                    ':codigo3' => $cod]);
    $rows = $stmt->fetchAll();

    $outofstock_lines = "";
    
    foreach ($rows as $row) {
        // threshold for being out of stock.
        $threshold = 12;

        echo $row['d'] . ' ' . $row['q'];
        $elts[$cod]["curstock"] -= $row['q'];
        echo ' ' . $elts[$cod]["curstock"];

        if ($is_out) {
            if ($elts[$cod]["curstock"] > $threshold) {
                $is_out = false;
                $outofstock_lines .= "Sem estoque de " . ISOToDmy($row['d']) .
                    " até " . ISOToDmy($current_end_date) . "\n";
            }
        }
        
        if ($elts[$cod]["curstock"] < $threshold) {
            echo " OUT OF STOCK HERE";
        }

        if ($elts[$cod]["curstock"] < $threshold && !$is_out) {
            $is_out = true;
            echo " OUT OF STOCK HERE";
            $current_end_date = $row['d'];
        }
        echo "\n";
    }
    
    // $largest_vendas = array_column($rows, "venda");
    // $elts[$cod]["largest"] = implode("\n", $largest_vendas);
    
    // echo "Trying $cod\n";
    // print_r($row);

    // if ($row["caixa"] < 1) {
    // echo "$row[caixa] $row[codigo] $row[disp] $row[resv] $row[caixa] $row[nome]<br>";
    // }

    echo $outofstock_lines;
}
