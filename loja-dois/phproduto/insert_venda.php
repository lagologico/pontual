<?php
require_once("../db.php");

$json = file_get_contents("php://input");
$sheet = json_decode($json, true);

// print_r($obj);
$lastRow = (int) $sheet['lastRow'];

$stmt = $dbh->prepare("insert into venda (numero, codigo, qtde) values (:numero, :codigo, :qtde)");

$dbh->beginTransaction();

echo $lastRow;
echo "\n";

for ($r = 1; $r <= $lastRow; $r++) {
    if (isset($sheet[$r . '-2'])) {
        $vendaNumero = $sheet[$r . '-2'];
        $vendaCodigo = $sheet[$r . '-3'];
        $vendaQtde = $sheet[$r . '-8'];

        echo "Attempt to insert $vendaCodigo $vendaQtde\n";

        try {
            $stmt->execute([':numero' => $vendaNumero,
                            ':codigo' => $vendaCodigo,
                            ':qtde' => $vendaQtde]);
        } catch (Exception $e) {
            echo $e;
        }
        
        ob_flush();
        // insertProduto($dbh, $rowCodigo, $rowNome, $rowDisp, $rowResv, 0);
    }
}

$dbh->commit();

addTimestamp($dbh, "insertVenda");
