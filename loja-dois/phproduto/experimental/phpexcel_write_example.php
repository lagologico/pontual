<?php
require_once("PHPExcel/PHPExcel.php");

include 'PHPExcel/PHPExcel/Writer/Excel2007.php';

$objPHPExcel = new PHPExcel();

$objPHPExcel->getDefaultStyle()->getFont()->setName('Arial');
$objPHPExcel->getDefaultStyle()->getFont()->setSize(10);
$objPHPExcel->setActiveSheetIndex(0);

function writeCell($obj, $ref, $val) {
    $obj->getActiveSheet()->setCellValueExplicit($ref, $val, PHPExcel_Cell_DataType::TYPE_STRING);
}

function writeBoldCell($obj, $ref, $val) {
    writeCell($obj, $ref, $val);
    setBold($obj, $ref);
}

function writeBoldAndCenterCell($obj, $ref, $val) {
    writeCell($obj, $ref, $val);
    setBold($obj, $ref);
    setCenter($obj, $ref);
}

function writeCenterCell($obj, $ref, $val) {
    writeCell($obj, $ref, $val);
    setCenter($obj, $ref);
}

function setBold($obj, $ref) {
    $obj->getActiveSheet()->getStyle($ref)->getFont()->setBold(true);
}

function setCenter($obj, $ref) {
    $ctr = array(
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
        );
    $obj->getActiveSheet()->getStyle($ref)->applyFromArray($ctr);
}

writeBoldCell($objPHPExcel, "A1", "Código");
writeBoldAndCenterCell($objPHPExcel, 'B1', "Disponível");
writeBoldAndCenterCell($objPHPExcel, "C1", "Reservado");
writeBoldAndCenterCell($objPHPExcel, "D1", "Chegando");
writeBoldAndCenterCell($objPHPExcel, "E1", "Vendas 2015");
writeBoldAndCenterCell($objPHPExcel, "F1", "Vendas 2016");
writeBoldAndCenterCell($objPHPExcel, "G1", "Por caixa");

writeBoldCell($objPHPExcel, "A2", "137350RR");
writeBoldAndCenterCell($objPHPExcel, "B2", "12.342");
writeCell($objPHPExcel, "A3", "Chaveiro de metal Redondo sem embalagem");


// Post Processing
// set column widths
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(11);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(11);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(11);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(14);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(11);

$objPHPExcel->getActiveSheet()->setPrintGridlines(true);
$objPHPExcel->getActiveSheet()->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(1, 1);

header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
header('Content-Disposition: attachment; filename="phpexcel_example.xlsx"');

$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
$objWriter->save("php://output");
