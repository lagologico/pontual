/* First three rows of Report item */
/* Estoque, caixa */
select prod.codigo, prod.disp, prod.resv, prod.caixa, prod.nome
from produto as prod
where prod.codigo = '139454'

/* PAC, only first container listed, though total for all is calculated */
/* COD. 143039A tem 2 PACs */
select concat(sum(qtde), " (", nome, ")") as pac_text from pac where codigo = '143039A'

/* VENDAS 2015, 2016 */
select prod.codigo,
sum(case when pedido.data between "2015-01-01" and "2015-12-31" then venda.qtde end) as vendas2015,
sum(case when pedido.data >= "2016-01-01" then venda.qtde end) as vendas2016
from produto as prod, pedido, venda
where pedido.numero = venda.numero
and venda.codigo = prod.codigo
and prod.codigo = '139454'


/* Subquery for ultimo container and qtde of given codigo
 Note that codigo is repeated. Need codigo1 and codigo2 for PHP PDO */
select concat("Último container - ", date_format(min(entrada.data), "%d/%m/%y"), " - ", entrada.nome, " - ", format(compra.qtde, 0, 'de_DE'), " pçs") as ultimo
from entrada, compra 
where compra.codigo = '139454'
and entrada.data > (select max(data) from compra where codigo = '139454');

select min(entrada.data), entrada.nome, compra.qtde from entrada, compra 
where compra.codigo = '139454'
and entrada.data > (select max(data) from compra where codigo = '139454');


/* Compile stock at times of venda to determine periods when out of stock */
/* TODO */

/* sales and purchases */
select pedido.data as d, -sum(venda.qtde) as q
from pedido, venda
where pedido.numero = venda.numero
and venda.codigo = '137350R'
group by d
union all
select data as d, qtde as q
from compra
where codigo = '137350R'
order by d desc


/* Largest three purchases in 2016 */
select concat(date_format(pedido.data, "%d/%m/%y"), " - ", substring_index(pedido.cliente, " ", 3), " - ", format(venda.qtde, 0, "de_DE"), " pçs") as venda
from pedido, venda
where pedido.numero = venda.numero
and venda.codigo = '143187A'
and pedido.data >= '2016-01-01'
order by venda.qtde desc limit 3;

/*
select pedido.data, pedido.cliente, venda.qtde from pedido, venda
where pedido.numero = venda.numero
and venda.codigo = '139454'
and pedido.data >= '2016-01-01'
order by venda.qtde desc limit 3;
*/
