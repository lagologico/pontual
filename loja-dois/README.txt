Loja Dois

functionality is in phproduto folder

Updating:

phproduto/config.php

$_CONFIG_THRESHOLD = 50; // if stock is below this number, it is
                         // considered "out of stock".
                         // Used in preliminary_report.php
$_MONTHS_BACK = 7; // used in determining if a stock is "low".
                   // used in formula
  // $sales_n_months_back = (int) ($_MONTHS_BACK * ($snapshot[$codigo]["vendas"] / 12));
  // that is, the average monthly sales over the last 12 months,
  // multiplied by $_MONTHS_BACK

Years queried (as of March 2017):
2016
2017
phpexcel_report_header.php
preliminary_report.php
report_csv.php
report_xlsx.php
sqls_for_report.php

----------

Duplica os dados da Loja Virtual para analisar o histórico de vendas.

Relatórios necessários:

Insert produto:
- Produtos > Conferência de Estoque > Selecionar "Todos"
- Produtos Site List_active_cod_csv.php

Update stock:
- Produtos > Conferência de Estoque > Selecionar "Todos"

Update caixa:
- Movimento > Lista de precos > Lista geral - ativos > Selecionar "Todos"

Insert pedido:
- Vendas por número > Sintético

Insert venda:
- Vendas por produto > Analítico

Insert entrada:
- Movimento > Precos > Lista - Container (preencher a mao)

Insert compra:
- Compras por data > Analítico

Insert PAC:
- N:\CHEGANDO
