Loja Dois

Baseando nos dados da Loja Virtual, a Loja Dois analisa o histórico de vendas.

Relatórios necessários:

Clientes > Lista de Clientes > Filtrar por Secundário

Produtos > Conferência de Estoque > Selecionar Todos

Vendas > Combinar Vendas por Data (Analítico) e Vendas por Cliente (Analítico)

