Produto
- codigo
- nome
- disp
- resv
- caixa

Pedido
- numero (unique int)
- data (int)
- cliente (text, first 40 chars)

Venda
- Pedido.numero  (FK)
- Produto.codigo (FK)
- qtde

PAC (deixar separado)
- Produto.codigo (FK)
- qtde
- nome (do container, text)

Entrada
- nome
- data (int)

Compra
- data (int)
- Produto.codigo (FK)
- qtde
