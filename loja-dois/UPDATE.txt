To update Loja Dois (every new year)

For example, to move 2015-2016 to 2016-2017

> grep "2015" *

For each matching file:

replace 2016 with 2017 (edit newer year first)
replace 2015 with 2016

phpexcel_report_header.php
preliminary_report.php
report_csv.php
report_xlsx.php
sqls_for_report.php
