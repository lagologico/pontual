<?php

require_once("../settings.php");
require_once("../db_conn.php");
require_once("normalize_chars.php");
require_once("html_util.php");

$page_title = "Busca";

if (isset($_GET['q'])) {
  $buscaOrig = htmlentities($_GET['q']);

  $busca = trim(preg_replace("/[^a-z0-9\s\/\-]+/", "", normalizeChars($_GET['q'])));

  // add wildcards on whitespace
  $busca = preg_replace("/\s/", "%", $busca);
  
  $content = "<div class='categoria_nome'>Busca: $buscaOrig</div>";

  // count total produtos for pagination
  $sql = "select count(*) as ct from v2_produto where normalizado like :query";
  $sth = $dbh->prepare($sql);
  $sth->execute([ ":query" => "%$busca%" ]);
  $result = $sth->fetch();
  $totalRows = $result['ct'];

  if ((int) $totalRows === 0) {
    $content .= "<div class='categoria_nome'>Nenhum produto encontrado.</div>";
  } else {
   
    $rowsPerPage = 12;
    $totalPages = ceil($totalRows / $rowsPerPage);

    if (isset($_GET['pg']) && is_numeric($_GET['pg'])) {
      $currentPage = (int) $_GET['pg'];
    } else {
      $currentPage = 1;
    }

    if ($currentPage > $totalPages) {
      $currentPage = $totalPages;
    }

    if ($currentPage < 1) {
      $currentPage = 1;
    }

    $offset = ($currentPage - 1) * $rowsPerPage;

    $previousPage = $currentPage - 1;
    $nextPage = $currentPage + 1;

    // current page (not a link)
    $content .= "<div class='busca_pagina'>Página $currentPage</div>";
    
    // Pagination links 
    $numPageLinks = 3;
    $buscaEncoded = urlencode($buscaOrig);

    $content .= "<div class='pages'>\n";
    
    if ($currentPage > 1) {
      $content .= "<a href='busca.php?q=$buscaEncoded&amp;pg=1' data-ajax='false'>Início</a>&nbsp;";
      $content .= "<a href='busca.php?q=$buscaEncoded&amp;pg=$previousPage' data-ajax='false'>&lt; Anterior</a>&nbsp;";
    } else {
      $content .= ""; // Início &lt; Anterior&nbsp;";
    }

    // numbered links
    $leftmostPage = max(1, $currentPage - $numPageLinks);
    for ($i = $leftmostPage; $i < $currentPage; $i++) {
      $content .= "<a href='busca.php?q=$buscaEncoded&amp;pg=$i' data-ajax='false'>$i</a>&nbsp;";
    }

    // current page (not a link)
    $content .= "$currentPage";

    $rightmostPage = min($totalPages, $currentPage + $numPageLinks);
    for ($i = $currentPage + 1; $i <= $rightmostPage; $i++) {
      $content .= "&nbsp;<a href='busca.php?q=$buscaEncoded&amp;pg=$i' data-ajax='false'>$i</a>";
    }

    if ($currentPage < $totalPages) {
      $content .= "&nbsp;<a href='busca.php?q=$buscaEncoded&amp;pg=$nextPage' data-ajax='false'>Próxima &gt;</a>";
      $content .= "&nbsp;<a href='busca.php?q=$buscaEncoded&amp;pg=$totalPages' data-ajax='false'>Fim</a>";
    } else {
      $content .= ""; // &nbsp;Próxima &gt; Fim";
    }

    $content .= "</div>"; 

    $sql = "select p.codigo, p.descricao, p.peso, p.medidas, p.preco, p.atualizado from v2_produto p
where p.normalizado like :query
order by p.codigo
limit $offset, $rowsPerPage";

    $sth = $dbh->prepare($sql);
    $sth->execute([ ":query" => "%$busca%" ]);
    $result = $sth->fetchAll();

    foreach ($result as $produto) {
      // print Card
      $content .= getCard($produto);
    }

    $content .= "<div class='pages'>\n";
    
    if ($currentPage > 1) {
      $content .= "<a href='busca.php?q=$buscaEncoded&amp;pg=1' data-ajax='false'>Início</a>&nbsp;";
      $content .= "<a href='busca.php?q=$buscaEncoded&amp;pg=$previousPage' data-ajax='false'>&lt; Anterior</a>&nbsp;";
    } else {
      $content .= ""; // Início &lt; Anterior&nbsp;";
    }

    // numbered links
    $leftmostPage = max(1, $currentPage - $numPageLinks);
    for ($i = $leftmostPage; $i < $currentPage; $i++) {
      $content .= "<a href='busca.php?q=$buscaEncoded&amp;pg=$i' data-ajax='false'>$i</a>&nbsp;";
    }

    // current page (not a link)
    $content .= "$currentPage";

    $rightmostPage = min($totalPages, $currentPage + $numPageLinks);
    for ($i = $currentPage + 1; $i <= $rightmostPage; $i++) {
      $content .= "&nbsp;<a href='busca.php?q=$buscaEncoded&amp;pg=$i' data-ajax='false'>$i</a>";
    }

    if ($currentPage < $totalPages) {
      $content .= "&nbsp;<a href='busca.php?q=$buscaEncoded&amp;pg=$nextPage' data-ajax='false'>Próxima &gt;</a>";
      $content .= "&nbsp;<a href='busca.php?q=$buscaEncoded&amp;pg=$totalPages' data-ajax='false'>Fim</a>";
    } else {
      $content .= ""; // &nbsp;Próxima &gt; Fim";
    }

    $content .= "</div>"; 

  }
} else {  // no query given
  $content = "<h3>Digite o código ou descrição acima, no campo de busca.</h3>";
}

require_once("template.php");

?>
