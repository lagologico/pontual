One Box Only

For typical usage, start at step 4.

1. Download list of codigos, mix of codigos from site and ativo (want to cover a reasonable set of products)

2. Get box qty for each item

3. Save codigos and box qty as listas/cod_cx.csv
"Codigo";"Qtde_por_caixa_grande"

Example:

137263;480
"137350A";480
...

4. Download Movimento > Lista de Precos > Lista ativo e reserva
Save as listas/estoqueYYYYMMDD.xls

5. Run Python script main.py. Items not present in estoqueYYYYMMDD.xls are likely 0.

6. Copy list of items with estoque less than MAX(24, one_box) into Loja-dois

7. Repeat 1-3 when new items arrive
