import xlrd
import csv
import os
import time

from collections import OrderedDict
from datetime import date

readme = """
One Box Only

QTDE_CX - UPDATE WHEN NEW PRODUCTS ARRIVE
1. Download list of codigos, a mix of site and ativo
2. Get box qty for each item
3. Save codigos and box qty as listas/cod_cx.csv
"Codigo";"Qtde_por_caixa_grande"

ESTOQUE
4. Download Movimento > Lista de Precos > Lista ativo e reserva
Save as listas/estoqueYYYYMMDD.xls

5. Run Python script. Items not present in estoqueYYYYMMDD.xls are likely 0.

6. Copy list of items with estoque less than MAX(24, one_box) into Loja-dois

7. Repeat 1-3 when new items arrive
"""


COD_CX = "C:/Users/heitor/Desktop/emacs-24.3/bin/one_box_only/listas/cod_cx.csv"
ESTOQUE_PREFIX = "C:/Users/heitor/Desktop/emacs-24.3/bin/one_box_only/listas/estoque"
RESULT_PREFIX = "C:/Users/heitor/Desktop/emacs-24.3/bin/one_box_only/results/low"

def main():
    date_postfix = date.today().strftime("%Y%m%d")
    cx = OrderedDict()
    est = OrderedDict()
    
    try:
        # get delimiter
        delim = ";"
        with open(COD_CX) as cod:
            line = next(cod)
            if line.find(delim) == -1:
                delim = ","
                
        with open(COD_CX) as cod:
            print("cod_cx created", time.ctime(os.path.getmtime(COD_CX)))
            reader = csv.reader(cod, delimiter=delim, quotechar='"')
            for row in reader:
                
                cx[row[0]] = int(row[1])
    except FileNotFoundError as err:
        print(err)
        print("Save codigos/caixa in " + COD_CX)

    # Download Movimento > Lista de Precos > Lista ativo e reserva as estoqueYYYYMMDD.xls
    ESTOQUE_XLS = ESTOQUE_PREFIX + date_postfix + ".xls"
    try:
        book = xlrd.open_workbook(ESTOQUE_XLS, logfile=open(os.devnull, 'w'))
        sh = book.sheet_by_index(0)
        nrows = sh.nrows
        for row in range(nrows):
            cod_cell = sh.cell_value(rowx=row, colx=0)
            try:
                disp_cell = int(sh.cell_value(rowx=row, colx=7))
            except ValueError:
                # print("Error getting disponivel cell on row", row)
                disp_cell = -1000000
            try:
                resv_cell = int(sh.cell_value(rowx=row, colx=13))
            except ValueError:
                # print("Error getting reserva cell on row", row)
                resv_cell = -1000000
            
            if isinstance(cod_cell, float):
                cod = str(int(cod_cell))
            
            elif isinstance(cod_cell, str):
                cod_cell = cod_cell.strip()
                if cod_cell == "" or cod_cell[0] == "C":
                    continue

                cod = cod_cell
            else:
                print(cod_type, "not recognized")

            est[cod] = disp_cell + resv_cell
            
        # compare estoques
        result_filename = RESULT_PREFIX + date_postfix + ".txt"
        with open(result_filename, 'w') as result:
            for cod in cx:
                if est.get(cod, 0) < cx[cod]:
                    print(cod, file=result)

            print("Wrote", result_filename)
        
    except FileNotFoundError as err:
        print(err)
        print("Make sure you have generated an Estoque file today")

# def test():
main()
